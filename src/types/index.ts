export interface FormTypes {
  email: string;
  password: string;
}

export interface StatisticsTypes {
  id: string;
  date: string;
  confirmed_diff: number;
  deaths_diff: number;
  active_diff: number;
}