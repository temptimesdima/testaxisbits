import { defineStore } from 'pinia'
import { FormTypes } from "../types/index"

export const useAuthStore = defineStore('auth', () => {
  const router = useRouter();
  const isAuth = ref(false)

  function login(payload: FormTypes) {
    try {
      // Save login to localStorage
      localStorage.setItem("login", JSON.stringify(payload));
      router.push('/dashboard')
    } catch (error) {
      console.log(error)
    }
  }

  function logout() {
    try {
      localStorage.removeItem("login");
      localStorage.removeItem("statistics");
      router.push('/')
    } catch (error) {
      console.log(error)
    }
  }

  return { isAuth, login, logout }
})