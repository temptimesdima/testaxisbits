import { defineStore } from 'pinia'
import { StatisticsTypes } from "../types/index"
import axios from "axios"
import { ref, Ref } from "vue";

export const useHistoryStore = defineStore('statistics', () => {
  const history: Ref<StatisticsTypes[]> = ref([])

  async function getData() {
    const date = new Date()
    const apiPromises = []

    for (let i = 0; i < 10; i++) {
      date.setDate(date.getDate() - 1)
      const dateString = date.toISOString().substr(0, 10)
      apiPromises.push(
        axios.get(`https://covid-api.com/api/reports/total?date=${dateString}&iso=UKR`)
      )
    }

    try {
      const results = await Promise.all(apiPromises)
      const data = results.filter(result => result.data.data.length !== 0).map(result => {
        const item = result.data.data;
        item.id = item.date; // add id property with date as value
        return item;
      });
      history.value = data
    } catch (error) {
      console.error(error)
    }
  }

  function deleteItem(id: string) {
    const index = history.value.findIndex(item => item.id === id);
    if (index > -1) {
      history.value.splice(index, 1);
    }
  }

  return { history, getData, deleteItem }
})
